﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugDrawSpehre : MonoBehaviour
{

    private void Update()
    {
       transform.position = new Vector3(transform.position.x,
           transform.position.y,
         Mathf.PingPong(Time.time, 3));
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.1f);
    }
}
