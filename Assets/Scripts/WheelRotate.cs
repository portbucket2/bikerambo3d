﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelRotate : MonoBehaviour
{
    [Range(0f, 200f)] public float rotateSpeed;

    private void Update()
    {
        transform.Rotate(new Vector3(10,0,0) * Time.deltaTime * rotateSpeed);
    }
}
