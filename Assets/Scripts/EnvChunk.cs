﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnvChunk : MonoBehaviour
{
    public string name;
    public int id;
    public GameObject chunkObject;
    public Transform backTrans;


    public bool isTargetInvoked;
    public Camera mainCam;

    public bool isCamSight;

    public List<Enemy> enemyList;
    public List<Transform> enemyPosList;
    

    private void Awake()
    {
        isCamSight = UtilityGM.IsTargetVisible(Camera.main, this.gameObject);
        for (int i = 2; i < 8; i++)
        {
            enemyList.Add(transform.GetChild(i).GetComponent<Enemy>());
        }
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        if (UtilityGM.IsTargetVisible(Camera.main, this.gameObject))
        {
            isCamSight = true;
        }
        else
        {
            if (!isTargetInvoked && isCamSight)
            {
                isTargetInvoked = true;
                
                EnvironmentMovment.Instance.ChunkRechedOnFinished(this.gameObject);
            }
        }
    }

    public void ResetIncreaseFactor()
    {
    }

    public void ActivateEnemyWhileGeneration()
    {
        int t_RandValue = Random.Range(3, enemyList.Count);

        for (int i = 0; i < 3; i++)
        {
            enemyList[i].ActivateEnemy();
            enemyList[i].transform.position = enemyPosList[i].position;
        }
    }
}
