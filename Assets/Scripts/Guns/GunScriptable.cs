﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Guns", order = 1)]
public class GunScriptable : ScriptableObject
{
    public string name;
    public Sprite gunSprite;
    public GameObject gunPrefab;
    public int ammoMagazine;
    public float reloadTime;
    public int totalAmmo;
}
