﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleToScreen : MonoBehaviour
{

    public Camera mainCam;

    private void Start()
    {
        Debug.LogError(UtilityGM.IsTargetVisible(mainCam,gameObject));
    }
}
