﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolController : MonoBehaviour
{
    public PoolController poolController;
    public WeaponIK weaponIK;


    private void Start()
    {
        StartCoroutine(ShootRoutine());
    }
    IEnumerator ShootRoutine()
    {
        yield return new WaitForSeconds(1.5f);
        while (true)
        {
            ShootTarget();
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void ShootTarget()
    {
        //Debug.Log(weaponIK.GetTarget());
        if (weaponIK.GetTarget().x > 0)
        {
            PlayerController.Instance.PlayLookFront();
        }
        else
        {
            PlayerController.Instance.PlayLookBack();
        }
        poolController.Shoot(weaponIK.GetTarget());
    }
}
