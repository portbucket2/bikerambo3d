﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnvironmentMovment : MonoBehaviour
{
    public Transform currentBackTrans;
    public Transform envParent;

    public List<EnvChunk> buildingLayerObjectList;
    public List<GameObject> roadsideLayerObjectList;


    public bool isMoving;
    [Range(0f, 10f)]
    public float moveSpeed;
    [Range(0f, 10f)]
    public float moveIncreaseFactor;

    public static EnvironmentMovment Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        InitLevelGen();
        StartMoving();
    }

    private void Update()
    {
        if (isMoving)
        {
            envParent.Translate(new Vector3(0, 0, moveSpeed * Time.deltaTime * moveIncreaseFactor));
        }
    }

    public void InitLevelGen()
    {
        for (int i = 0; i < 5; i++)
        {
            Vector3 pos = new Vector3(0f, 0f, -(i * 47.97f));
            GameObject go = Instantiate(SelectNextBuildingLayer().chunkObject, pos, Quaternion.identity, envParent);
            go.transform.localPosition = pos;
            go.transform.localRotation = Quaternion.identity;

            go.GetComponent<EnvChunk>().ActivateEnemyWhileGeneration();

            if(i == 4)
            {
                currentBackTrans = go.transform;
            }
        }
     }
    
    private void StartMoving()
    {
        isMoving = true;
    }

    public void ChunkRechedOnFinished(GameObject game)
    {
        StartCoroutine(WaitAndDestroy(game));
        //Called from EnvChunk Event
        Debug.LogWarning("Chunk Reached to finished point");
       
        Vector3 pos = currentBackTrans.localPosition + new Vector3(0f, 0f, -47.97f);
        GameObject go = Instantiate(SelectNextBuildingLayer().chunkObject,pos,Quaternion.Euler(new Vector3(0,-90,0)),envParent);

        go.transform.position = Vector3.zero;
        go.transform.localPosition = currentBackTrans.localPosition + new Vector3(0f, 0f, -47.97f);
        currentBackTrans = go.transform;

        go.GetComponent<EnvChunk>().ActivateEnemyWhileGeneration();
    }

    IEnumerator WaitAndDestroy(GameObject go)
    {
        yield return new WaitForSeconds(8f);
        Destroy(go);
    }


    public void IncreaseMovementFactor()
    {
        moveIncreaseFactor = Mathf.Lerp(moveIncreaseFactor, 4 ,Time.deltaTime*2f);
    }

    public void ResetMovementFactor()
    {
        StartCoroutine(ResetFactorRoutine());
    }

    private IEnumerator ResetFactorRoutine()
    {
        while (moveIncreaseFactor>1.5f)
        {
            moveIncreaseFactor -= Time.deltaTime * 1.5f;
            yield return new WaitForEndOfFrame();
        }
    }

    public EnvChunk SelectNextBuildingLayer()
    {
        EnvChunk t_SelectedBuildingLayer;

        int t_RandValue = Random.Range(0, buildingLayerObjectList.Count);

        t_SelectedBuildingLayer = buildingLayerObjectList[t_RandValue];

        return t_SelectedBuildingLayer;
    }

    public GameObject SelectNextRoadSideLayer()
    {
        GameObject t_SelectedBuildingLayer = null;

        int t_RandValue = Random.Range(0, roadsideLayerObjectList.Count);

        t_SelectedBuildingLayer = roadsideLayerObjectList[t_RandValue];

        return t_SelectedBuildingLayer;
    }
}

