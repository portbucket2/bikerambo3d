﻿using UnityEngine;
using UnityEditor;

// replace "SpawnZone" with the class of your Script on a GameObject
[CustomEditor(typeof(PlayerController))]
public class SpawnZoneInspector : Editor
{

	// replace "SpawnZone" with the class of your Script on a GameObject
	private PlayerController myZone;

	public void OnEnable()
	{
		// replace "SpawnZone" with the class of your Script on a GameObject
		myZone = target as PlayerController;
	}


	void OnSceneGUI()
	{
		// Choose a Color
		Handles.color = Color.red;

		// replace "SpawnZone" with the class of your Script on a GameObject
		// your Class has to have a radius
		Handles.DrawWireDisc(this.myZone.transform.position, Vector3.forward, this.myZone.spawnRadius);

		Handles.DrawWireArc(myZone.transform.position, new Vector3(0,1,0), myZone.transform.position, 360, myZone.spawnRadius);
		myZone.AdjustCollider();
	}

}
