﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public Rigidbody playerRigidbody;
    public Animator playerAnimator;

    public List<Enemy> visibleTargetList;

    public PoolController poolController;
    public WeaponIK weaponIK;
    public bool isWaitTimeFinished;
    [Range(0f, 1f)] public float bulletWaitTime;

    private int LOOK_FRONT = Animator.StringToHash("pistolfront");
    private int LOOK_BACK = Animator.StringToHash("pistolback");

    void Awake()
    {
        _initialLocalEulerAngle = cameraTransformReference.localEulerAngles;

        if (Instance == null)
        {
            Instance = this;
        }

        playerRigidbody = GetComponent<Rigidbody>();
        collider = GetComponent<SphereCollider>();
    }

    const float inputSpeedPremultiplier = 1.1f;
    const float referenceHeight_px = 2400;
    float standardizedPixelCount;


    private void Start()
    {
        standardizedPixelCount = (Screen.height / referenceHeight_px) / inputSpeedPremultiplier;
        OnLevelStarted();
        ActivateTouch();
    }


    public void OnLevelStarted()
    {
        TouchGM.Instance.EnableTouchController();
        TouchGM.Instance.OnTouchDown += OnTouchDown;
        TouchGM.Instance.OnTouch += OnTouch;
        TouchGM.Instance.OnTouchUp += OnTouchUp;
    }

    public void OnLevelEnded()
    {
        TouchGM.Instance.OnTouchDown -= OnTouchDown;
        TouchGM.Instance.OnTouch -= OnTouch;
        TouchGM.Instance.OnTouchUp -= OnTouchUp;
        TouchGM.Instance.DisableTouchController();
    }

    

    [Header("External Reference :   Camera")]
    public Transform cameraTransformReference;

    [Space(5.0f)]
    [Range(1, 100)]
    public float validTouchDistance = 1f;
    [Range(0.0f, 10f)]
    public float lookAtVelocity = 0;
    [Range(1, 100)]
    public float forwardSpeed;
    [Range(1, 500)]
    public float backwardSpeed;

    public Vector2 movementBarrier;
    [Range(0.0f, 10f)]
    public float spawnRadius;
    public SphereCollider collider;

    private bool _isTouchActive = false;

    private Vector3 _initialLocalEulerAngle;
    private Vector3 _touchDownPosition;




    private void Update()
    {
        if (transform.position.x >= movementBarrier.x)
        {
            transform.position = new Vector3(movementBarrier.x, transform.position.y, transform.position.z);
        }
        if (transform.position.x <= -movementBarrier.x)
        {
            transform.position = new Vector3(-movementBarrier.x, transform.position.y, transform.position.z);
        }

        for (int i = 0; i < visibleTargetList.Count; i++)
        {
            if(visibleTargetList[i] != null)
            {
                float t_Distance = Vector3.Distance(visibleTargetList[i].transform.position, transform.position);
                //Debug.LogError(t_Distance);

                if(t_Distance < 15f && isWaitTimeFinished)
                {
                    weaponIK.SetTargetTransform(visibleTargetList[i].transform);
                    StartCoroutine(ShootRoutine(visibleTargetList[i].transform.position));
                }
            }
        }
    }

    IEnumerator ShootRoutine(Vector3 pos)
    {
        isWaitTimeFinished = false;
        ShootTarget();
        yield return new WaitForSeconds(bulletWaitTime);
        isWaitTimeFinished = true;
    }
    public void ShootTarget()
    {
       // Debug.Log(weaponIK.GetTarget());
        

        if (weaponIK.GetTarget().x > 0)
        {
            //Debug.LogError("FRONT");
            PlayLookFront();
        }
        else
        {
           // Debug.LogError("BACK");
            PlayLookBack();
        }

        poolController.Shoot(weaponIK.GetTarget());
    }


    private void OnTouchDown(Vector3 touchPosition)
    {
        _touchDownPosition = touchPosition;
    }

    private void OnTouch(Vector3 touchPosition)
    {

        if (_isTouchActive)
        {
            _touchDownPosition = touchPosition;

            
            playerRigidbody.AddForce(Vector3.right * forwardSpeed*Time.fixedDeltaTime, ForceMode.VelocityChange);

            EnvironmentMovment.Instance.IncreaseMovementFactor();

            
        }
        else
        {
            if (Vector3.Distance(_touchDownPosition, touchPosition) >= validTouchDistance)
            {
                // _isTouchActive = true;
            }
        }
    }

    private void OnTouchUp(Vector3 touchPosition)
    {
        playerRigidbody.velocity = Vector3.zero;
        playerRigidbody.AddForce(-Vector3.right * backwardSpeed * Time.fixedDeltaTime, ForceMode.VelocityChange);
        EnvironmentMovment.Instance.ResetMovementFactor();
    }

    public float GetPlayerValueClamp()
    {
        float t_CurrentPosValue = transform.position.x;

        float t_ClampValue = Mathf.Clamp(t_CurrentPosValue,-movementBarrier.x,movementBarrier.x);

        float t_Clamp01 = Mathf.Clamp01(t_ClampValue);

        return t_Clamp01;
    }



    public void DeactivateTouch()
    {
        _isTouchActive = false;
    }
    public void ActivateTouch()
    {
        _isTouchActive = true;
    }


    public void AdjustCollider()
    {
        collider.radius = 1.9f * spawnRadius;
    }

    public void PlayLookFront()
    {
        playerAnimator.SetTrigger(LOOK_FRONT);
    }
    public void PlayLookBack()
    {
        playerAnimator.SetTrigger(LOOK_BACK);
    }

    public void SetAsVisibleTarget(Enemy enemy)
    {
        for (int i = 0; i < visibleTargetList.Count; i++)
        {
            if(visibleTargetList[i] == null)
            {
                visibleTargetList.RemoveAt(i);
            }
        }
        visibleTargetList.Add(enemy);
    }
    public void RemoveFromVisibleTarget(Enemy enemy)
    {
        
    }
}
