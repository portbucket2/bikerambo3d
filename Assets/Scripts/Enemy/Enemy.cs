﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Animator enemyAnimator;
    public Transform targetPlayer;

    public PoolController poolController;
    public WeaponIKEnemy weaponIK;
    public bool isVisible;
    public bool isWaitTimeFinished;
    [Range(0f, 1f)] public float bulletWaitTime;
    [Range(0f, 20f)] public float targetDistance;

    public GameObject poolObject;
    public bool isSetAsTarget;

    public ParticleSystem burstParticle;

    public List<Collider> capsulColliderList;
    


    private string BALCONY_PISTOL = "balconypistol";


    private void Awake()
    {
        targetPlayer = PlayerController.Instance.transform;
        poolController.poolObject = poolObject;

        enemyAnimator.transform.GetComponent<Collider>().enabled = true;
        if (capsulColliderList.Count == 0)
        {
            Collider[] c = enemyAnimator.transform.GetComponentsInChildren<Collider>();
            if (c.Length > 0) capsulColliderList.AddRange(c);
        }
    }
    private void Start()
    {
        for (int i = 1; i < capsulColliderList.Count; i++)
        {
            capsulColliderList[i].enabled = false;
        }
    }

    private void Update()
    {
        enemyAnimator.SetFloat(BALCONY_PISTOL, PlayerController.Instance.GetPlayerValueClamp());

        float distance = Vector3.Distance(this.transform.position, targetPlayer.position);
       

        if (UtilityGM.IsTargetVisible(Camera.main, this.gameObject))
        {
            isVisible = true;
        }
        else
        {
            isVisible = false;
        }

        if(isVisible && distance < targetDistance && isWaitTimeFinished)
        {
            StartCoroutine(ShootRoutine());
        }

        if(isVisible && !isSetAsTarget)
        {
            isSetAsTarget = true;
            PlayerController.Instance.SetAsVisibleTarget(this);
        }

        if(isVisible == false && isSetAsTarget)
        {
            PlayerController.Instance.RemoveFromVisibleTarget(this);
        }
    }

    IEnumerator ShootRoutine()
    {
        isWaitTimeFinished = false;
        poolController.Shoot(weaponIK.GetTarget());
        yield return new WaitForSeconds(bulletWaitTime);
        isWaitTimeFinished = true;
    }

    public void ActivateEnemy()
    {
        gameObject.SetActive(true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("ammo"))
        {
            Debug.Log(collision.transform.name);
            burstParticle.Play();
            //collision.transform.GetComponent<Animator>().enabled = false;
        }
    }
    public void ActivateRagdoll()
    {
        capsulColliderList[0].enabled = false;
        for (int i = 1; i < capsulColliderList.Count; i++)
        {
            capsulColliderList[i].enabled = true;
        }
    }
}
