﻿public enum GunType
{
    NONE,
    PISTOL,
    RIFLE,
    ROCKET_LAUNCHER
}