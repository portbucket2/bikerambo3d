﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PoolState
{
    ACTIVE,
    DEACTIVATE
}

public class PoolingItem : MonoBehaviour
{
    public int id;
    public PoolState poolState;

    public Rigidbody rigidbody;
    [Range(0f, 100f)] public float itemMoveSpeed;

    public ParticleSystem burstParticle;

    public bool isPlayer;


    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
//        burstParticle = transform.GetChild(0).GetComponent<ParticleSystem>();
    }


    public bool IsActive()
    {
        bool t_Result = false;
        if (poolState == PoolState.ACTIVE)
        {
            t_Result = true;
        }
        return t_Result;
    }
   
    public bool IsDeactivate()
    {
        bool t_Result = false;
        if (poolState == PoolState.DEACTIVATE)
        {
            t_Result = true;
        }
        return t_Result;
    }

    public void Move(Vector3 t_TargetPos)
    {
        gameObject.SetActive(true);
        poolState = PoolState.ACTIVE;

        rigidbody.AddForce(t_TargetPos * itemMoveSpeed, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isPlayer)
        {
            if (collision.transform.CompareTag("enemy"))
            {
                if (collision.transform.parent.GetComponent<Enemy>().enemyAnimator != null)
                {
                    collision.transform.parent.GetComponent<Enemy>().enemyAnimator.enabled = false;
                    collision.transform.parent.GetComponent<Enemy>().ActivateRagdoll();
                }
                poolState = PoolState.DEACTIVATE;
                gameObject.SetActive(false);
                transform.localPosition = Vector3.zero;
                Debug.Log("COLLISIN");
            }
        }
    }

    IEnumerator WaitAndDeactivate(Vector3 pos)
    {
        burstParticle.transform.localPosition = pos;
        burstParticle.Play();

        yield return new WaitForSeconds(1f);
        
    }
}
