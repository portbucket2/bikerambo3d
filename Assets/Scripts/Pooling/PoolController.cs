﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolController : MonoBehaviour
{
    public Transform poolObjectHolder;

    [Range(0, 500)]
    public int poolObjectNumber;
    public GameObject poolObject;

    public List<PoolingItem> poolingItemList;
    public ParticleSystem burstParticle;

    public static PoolController Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }



    private void Start()
    {
        InstantiateObject();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Shoot();
        }
    }

    public void Shoot(Vector3 t_TargetPos)
    {
        StartCoroutine(ShootRoutine(t_TargetPos));
    }
    IEnumerator ShootRoutine(Vector3 t_TargetPos)
    {
        PoolingItem poolingItem = PoolItem();
        yield return new WaitForEndOfFrame();
        if (poolingItem != null)
        {
            poolingItem.Move(t_TargetPos);
        }
    }

    public void InstantiateObject()
    {
        for (int i = 0; i < poolObjectNumber; i++)
        {
            GameObject t_Object = Instantiate(poolObject);
            t_Object.transform.SetParent(poolObjectHolder);
            t_Object.transform.localPosition = Vector3.zero;
            t_Object.SetActive(false);

            PoolingItem t_Pooling = t_Object.GetComponent<PoolingItem>();
            t_Pooling.poolState = PoolState.DEACTIVATE;
            poolingItemList.Add(t_Pooling);
        }
    }

    public PoolingItem PoolItem()
    {
        PoolingItem poolingItem = null;

        for (int i = 0; i < poolObjectNumber; i++)
        {
            if (poolingItemList[i].IsDeactivate())
            {
                poolingItem = poolingItemList[i];
                break;
            }
        }

        //if(poolingItem == null)
        //{
        //    PoolItem();
        //}

        return poolingItem;
    }

    public void BurstParticle(Vector3 pos)
    {
        burstParticle.transform.position = pos;
        burstParticle.Play();
    }

}
