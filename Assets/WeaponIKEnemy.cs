﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponIKEnemy : MonoBehaviour
{
    public Transform targetTransform;
    public Transform aimTransform;


    public int iterations = 10;
    [Range(0f, 1f)]
    public float weight = 1f;

    public float angleLimit = 90f;
    public float distanceLimit = 1.5f;



    public HumanBone[] humanBones;
    Transform[] boneTransforms;


    private void Awake()
    {

    }

    private void Start()
    {
        targetTransform = PlayerController.Instance.transform;
        Animator animator = GetComponent<Animator>();
        boneTransforms = new Transform[humanBones.Length];
        for (int i = 0; i < boneTransforms.Length; i++)
        {
            boneTransforms[i] = animator.GetBoneTransform(humanBones[i].bone);
        }
    }

    private Vector3 GetTargetPosition()
    {
        Vector3 targetDirection = targetTransform.position - aimTransform.position;
        Vector3 aimDirection = aimTransform.forward;
        float blendOut = 0.0f;


        float targetAngle = Vector3.Angle(targetDirection, aimDirection);
        if (targetAngle > angleLimit)
        {
            blendOut += (targetAngle - angleLimit) / 50f;
        }

        float targetDistance = targetDirection.magnitude;
        if (targetDistance < distanceLimit)
        {
            blendOut += distanceLimit - targetDistance;
        }

        Vector3 direction = Vector3.Slerp(targetDirection, aimDirection, blendOut);
        return aimTransform.position + direction;
    }



    private void LateUpdate()
    {
        Vector3 targetPosition = GetTargetPosition();

        for (int i = 0; i < iterations; i++)
        {
            for (int j = 0; j < boneTransforms.Length; j++)
            {
                Transform bone = boneTransforms[j];
                float boneWeight = humanBones[j].weight * weight;
                AimAtTarget(bone, targetPosition, boneWeight);
            }
        }
    }

    private void AimAtTarget(Transform bone, Vector3 targetPosition, float t_weight)
    {
        Vector3 aimDirection = aimTransform.forward;
        Vector3 targetDirection = targetPosition - aimTransform.position;
        Quaternion aimTorwards = Quaternion.FromToRotation(aimDirection, targetDirection);
        Quaternion blendedRotation = Quaternion.Slerp(Quaternion.identity, aimTorwards, weight);

       // bone.LookAt(targetTransform.position);
        bone.rotation = blendedRotation * bone.rotation;
    }



    public void SetTargetTransform(Transform target)
    {
        targetTransform = target;
    }
    public void SetAimTransform(Transform target)
    {
        targetTransform = target;
    }
    public Vector3 GetTarget()
    {
        return targetTransform.position - aimTransform.position;
    }
}
